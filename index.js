const express = require('express'),
path = require('path')

const app = express()
app.use(express.json())

const users = require('./db_users.json')
const geolib = require('geolib')
const nodemailer = require('./mailer')
const ports = require('sea-ports')
const { v4: uuidv4 } = require('uuid');
const { time } = require('console')
const _ = require('lodash');

for (let i in ports.JSON) {
    ports.JSON[i].gasPrice = _.ceil(_.random(1,5, true), 2);
}

const checkIfUserExist  = (newUser) => {
    let exists = 0;

    users.accounts.forEach((user) => {
        if (user.username === newUser.username) {
            console.log(user.username + "already exist")
            exists++;
        }

        if (user.email === newUser.email ) {
            console.log(user.email + "already exist")
            exists++
        }
    })

    return exists > 0;
}

const getRandomInt = (max = 999999) => {
    return Math.floor(Math.random() * max);
}

app.use((req, res, next) => {
    console.log("Requete")
    next();
})
app.get('/users', function(req, res) {
    res.json(users["accounts"])
  })

app.get('/user/:email', (req,res) => {
    const email = req.params.email
    const user = users["accounts"].find(user => user.email === email)
    res.status(200).json(user)
})

app.post('/user', (req,res) => {
    console.log(req.body)
    const newUser = req.body;

    if (true === checkIfUserExist(newUser)) {
        res.json({success: false, error: "User with this username/email already exist"});
        return;
    }

    newUser.id = uuidv4();
    newUser.tempRegistrationCode = getRandomInt();
    newUser.searchRadius = 13;
    newUser.unit = "km";
    newUser.verified = false
    users["accounts"].push(newUser)
    nodemailer.sendMail(newUser)
    res.json({success: true, user: newUser})
})

app.post('/user-verification', (req,res) => {
    const userid = req.body.userId;
    const verificationcode = parseInt(req.body.verificationCode);
    const User = users["accounts"].find(user => user.id === userid)

    if (verificationcode === User.tempRegistrationCode){
        const token = uuidv4();
        User.verified = true;
        User.token = token;
        users["accounts"].find(user => user.id === userid)["verified"] = true;
        users["accounts"].find(user => user.id === userid)["token"] = token;

        res.json({success: true, user: User})
    }
    else{
        res.json({success: false})
    }
})

app.get('/ports', (req,res) => {
    res.json(ports.JSON)
})

app.get('/ports-in-radius/:currentlat/:currentlon/:radius', (req,res) => {

    let ports_near = []
    let radius = parseFloat(req.params.radius);

    for (let i in ports.JSON) {
        if (ports.JSON[i].coordinates !== undefined){
            let point = {latitude: ports.JSON[i].coordinates[0], longitude: ports.JSON[i].coordinates[1]}
            let centerpoint = {latitude: parseFloat(req.params.currentlat),longitude: parseFloat(req.params.currentlon) }
            if(geolib.isPointWithinRadius(point, centerpoint, radius * 1000) === true){
                let distance = geolib.getDistance(centerpoint, point)
                let monJson = ports.JSON[i]
                monJson.distance = distance / 1000
                monJson.id = i;
                ports_near.push(monJson)
            }
        }
    }

    console.log(ports_near);
    res.json(ports_near)
})

const searchPorts = (req, res) => {
    const portName = req.params.name ? req.params.name.toLowerCase() : "";
    const latitude = parseFloat(req.params.currentlat);
    const longitude = parseFloat(req.params.currentlon);

    console.log({latitude, longitude, portName})
    let results = [];

    for (i in ports.JSON) {
        if(ports.JSON[i].name.toLowerCase().indexOf(portName) !== -1 && ports.JSON[i].coordinates !== undefined) {
            let result = ports.JSON[i];
            let point = {latitude: ports.JSON[i].coordinates[0], longitude: ports.JSON[i].coordinates[1]}
            let centerpoint  = {latitude, longitude};
            result.distance = geolib.getDistance(centerpoint, point) / 1000;
            result.id = i;
            results.push(result);
        }
    }

    res.json(results);
}

app.get('/ports/search/:currentlat/:currentlon/:name', searchPorts)
app.get('/ports/search/:currentlat/:currentlon/', searchPorts)

app.put('/ports/:id', (req,res) => {
    const portId = req.params.id;
    console.log(portId)
    const price = parseFloat(req.body.price);
    ports.JSON[portId].gasPrice = price;

    res.json({success: true, price})
})

app.post('/authenticate', (req,res) => {
    console.log(users.accounts)
    const email = req.body.email;
    const password = req.body.password;
    const user = users["accounts"].find(user => user.email === email);

    if (undefined === user) {
        console.log("Not found")
        res.json({success: false})
        return;
    }

    if (false === user.verified) {
        res.json({success: false, error: 'not-verified', user})
        return;
    }

    if (user.password === password) {
        res.json([{success: true, token: uuidv4(), user}])
        return;
    }

    console.log("Password not correct")
    res.json({success: false})
})

app.put("/user/:id", (req, res) => {
    console.log(req.body)
    const userId = req.params.id;
    let user = users.accounts.find(user => user.id === userId);

    if (undefined === user) {
        res.json({success: false, error: 'User do not exist'})
        return;
    }

    users.accounts.find(user => user.id === userId).searchRadius = parseInt(req.body.distance);
    users.accounts.find(user => user.id === userId).unit = req.body.unit.toLowerCase();
    user = users.accounts.find(user => user.id === userId);

    res.json({success: true, user})
})


app.listen(3000)

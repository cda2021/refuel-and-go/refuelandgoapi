module.exports = {
  sendMail: function(user){
    var mailOptions = {
      from: 'no-reply-refuelandgo@gmail.com',
      to: user.email,
      subject: '[REFUELANDGO] Votre code de verification',
      text: `Votre code est : ${user.tempRegistrationCode} !`,
      secure: false,
    };

    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
  }


};


var nodemailer = require('nodemailer');
const MailDev = require('maildev');

const maildev = new MailDev()

maildev.listen()

maildev.on('new', function (email) {
  // We got a new email!
})

var transporter = nodemailer.createTransport({
  host: "localhost",
  port: 1025,
  service: 'mail',
  secure: false, // true for 465, false for other ports
  auth: {
    user: 'no-reply-refuelandgo@mail.com',
    pass: 'test'
  },
    tls: {
        rejectUnauthorized: false
    }
});

